[![TakshakRamteke](./images/banner.png)](https://takshakramteke.github.io)

### 👏 Little About [me](https://takshakramteke.vercel.app) :

🧔 I'm a Freelancer Fullstack dev / designer from :india:, </br>
👨‍🎓 currently pursuing Bachelors in Computer Science and Engineering.<br/>
🖥️ I'm really passionate about learning new Stuff about computers and technology in general.<br/>
💙 In love with Anime, Manga.<br/>
🎨 Also love making Sketche's sometimes.

### 🤙 Connect with me:

<a href="https://twitter.com/TakshakRamteker">
    <img src="https://img.shields.io/badge/twitter-%231DA1F2.svg?&style=for-the-badge&logo=twitter&logoColor=white" />
</a>

<a href="https://dribbble.com/TakshakRamteke">
    <img src="https://img.shields.io/badge/Dribbble-EA4C89?style=for-the-badge&logo=dribbble&logoColor=white" />
</a>

<a href="takshakramteke0708@gmail.com?subject=Hello%20Takshak,">
    <img src="https://img.shields.io/badge/gmail-%23D14836.svg?&style=for-the-badge&logo=gmail&logoColor=white" />
</a>

<a href="https://www.linkedin.com/in/takshak-ramteke-15b840206/">
    <img src="https://img.shields.io/badge/linkedin-%230077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white" />
</a>

<a href="https://www.instagram.com/takshak_ramteke/">
    <img src ="https://img.shields.io/badge/Instagram-E4405F?style=for-the-badge&logo=instagram&logoColor=white" />
</a>

<br/>

### & Here's [My Resume](https://drive.google.com/file/d/1Z87Dpp15xVoGKV46Zhis7ekKw99HubnB/view?usp=sharing),
